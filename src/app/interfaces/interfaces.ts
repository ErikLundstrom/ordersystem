export interface foodOrder {
    orderID?: string,
    Table: string,
    EventID: string,
    Time: Date,
    Foods: Food[];
    Status?: string;
}
export interface drinkOrder {
    orderID?: string,
    Table: string,
    EventID: string,
    Time: Date,
    Drinks: Drink[];
    Status?: string;
}

export interface Dish {
    Title: string,
    Type: string,
    Allergies?: string[]
    Amount?: number;
}
export interface Food {
    Type: string,
    Allergies?: {}
}
export interface Drink {
    Title: string,
    Type: string,
    Special?: string[]
}

export interface FredaEvent {
    id?: string;
    Title: string,
    Date: Date,
    Time: string,
    Dishes?: Dish[],
    stats?: string,
    userAccess: string[],
}
export interface foodCounter {
    meat: number,
    fish: number,
    veg: number,
    dessert: number,
}
export interface drinkCounter {
    beer: number,
    wine: number,
    cocktail: number,
    alcFree: number,
}
export interface foodStats {
    Date: Date,
    Time: string,
    eventID: string,
    meat: number,
    fish: number,
    veg: number,
    dessert: number,
}
export interface User {
    uid: string,
    lastLogin?: Date,
    email: string
    role?: string
}