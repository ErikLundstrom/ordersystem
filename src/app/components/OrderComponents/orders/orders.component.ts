import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FredaEvent, foodOrder, drinkOrder } from 'src/app/interfaces/interfaces';
import { DatabaseService } from 'src/app/services/database.service';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.sass']
})
export class ordersComponent implements OnInit,OnDestroy {
  @Input() fredaEvent: FredaEvent;
   tab: boolean = true;
   foodOrders: foodOrder[];
   drinkOrders: drinkOrder[]
   Subscriptions: Subscription[] = []

  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.Subscriptions.push(this.db.ngUnsubscribe$.subscribe(x => this.unSubscribe()))
    this.Subscriptions.push(this.db.getFoodOrders(this.fredaEvent.id).subscribe(x => this.foodOrders = x));
    this.Subscriptions.push(this.db.getDrinkOrders(this.fredaEvent.id).subscribe(y => this.drinkOrders = y));
  }
  ngOnDestroy(){
    this.unSubscribe();
  }
  unSubscribe(){
    this.Subscriptions.map(x => x.unsubscribe());
  }
  toggleTab(bool:boolean){
    this.tab = bool;
  }

}