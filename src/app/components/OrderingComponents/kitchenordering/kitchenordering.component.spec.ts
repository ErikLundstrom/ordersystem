import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenorderingComponent } from './kitchenordering.component';

describe('KitchenorderingComponent', () => {
  let component: KitchenorderingComponent;
  let fixture: ComponentFixture<KitchenorderingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitchenorderingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenorderingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
