import { TestBed } from '@angular/core/testing';

import { CountDishesService } from './count-dishes.service';

describe('CountDishesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CountDishesService = TestBed.get(CountDishesService);
    expect(service).toBeTruthy();
  });
});
