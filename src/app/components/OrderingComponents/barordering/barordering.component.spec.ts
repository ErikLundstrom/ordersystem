import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarorderingComponent } from './barordering.component';

describe('BarorderingComponent', () => {
  let component: BarorderingComponent;
  let fixture: ComponentFixture<BarorderingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarorderingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarorderingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
