import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { orderingComponent } from './ordering.component';

describe('orderingComponent', () => {
  let component: orderingComponent;
  let fixture: ComponentFixture<orderingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ orderingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(orderingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
