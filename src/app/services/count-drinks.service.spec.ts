import { TestBed } from '@angular/core/testing';

import { CountDrinksService } from './count-drinks.service';

describe('CountDrinksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CountDrinksService = TestBed.get(CountDrinksService);
    expect(service).toBeTruthy();
  });
});
