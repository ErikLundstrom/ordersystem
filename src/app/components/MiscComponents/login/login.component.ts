import { Component, OnInit } from  '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  resetPass = false;

  constructor(public  authService:  AuthService) { }

  ngOnInit() {
  }
  toggleReset(){
    this.resetPass = !this.resetPass;
  }

}
