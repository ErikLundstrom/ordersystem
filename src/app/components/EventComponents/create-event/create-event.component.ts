import { Component, OnInit } from '@angular/core';
import { FredaEvent, Dish, User } from 'src/app/interfaces/interfaces';
import { DatabaseService } from 'src/app/services/database.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router'
import bulmaCalendar from "node_modules/bulma-calendar/dist/js/bulma-calendar.min.js";



@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.sass']
})
export class CreateEventComponent implements OnInit {
  Dishes: Dish[] = [];
  private icons = { meat: 'drumstick-bite', fish: 'fish', veg: 'carrot', dessert: 'ice-cream' };
  public show: boolean[] = [];
  inputs: string[] = [];
  objectKeys = Object.keys;
  loading: boolean = false;
  users: User[] = [];
  userAccess: string[] = [];

  public eventForm = new FormGroup({
    Title: new FormControl('', Validators.required),
    Date: new FormControl('', Validators.required),
    Type: new FormControl('Type'),
    FoodTitle: new FormControl(''),
  });
  constructor(private router: Router, private db: DatabaseService) { }

  ngOnInit() {
    this.datePicker();
    this.db.getUsers().subscribe(users => this.users = users);
  }
  datePicker() {
    // Initialize all input of date type.
    const calendars = bulmaCalendar.attach('[type="date"]');
    // Loop on each calendar initialized
    for (var i = 0; i < calendars.length; i++) {
      // Add listener to date:selected event
      calendars[i].on('select', (datepicker => {
        this.eventForm.get('Date').setValue(datepicker.data.value());
      }));
    }
  }
  addDish() {
    if (this.eventForm.get('FoodTitle').value && this.eventForm.get('Type')) {
      const Dish: Dish = {
        Type: (this.eventForm.get('Type').value).toString(),
        Title: (this.eventForm.get('FoodTitle').value).toString(),
        Allergies: [],
        Amount: 20
      }
      this.inputs.push('')
      this.Dishes.push(Dish);
      this.eventForm.get('Type').setValue('Type');
      this.eventForm.get('FoodTitle').reset();
    } else {
      alert('You need to choose a type and a title for the dish.')
    }
  }
  deleteDish(index: number) {
    this.Dishes.splice(index, 1);
    this.show.splice(index, 1);
  }
  addFoodPref(index: number) {
    this.Dishes[index].Allergies.push(this.inputs[index]);
    this.inputs[index] = ''
  }
  deleteFoodPref(index1: number, index2: number) {
    this.Dishes[index1].Allergies.splice(index2, 1);
  }
  addAmount(index: number) {
    this.Dishes[index].Amount++;
  }
  decreaseAmount(index: number) {
    if (this.Dishes[index].Amount > 0) {
      this.Dishes[index].Amount--;
    }
  }

  createEvent() {
    let value = true;
    this.Dishes.map(x => { if (!x.Amount) { value = false } })
    console.log(this.eventForm.valid);
    if (this.eventForm.valid && value == true && this.Dishes.length > 0 && this.userAccess.length > 0) {
      console.log(this.eventForm.valid);
      const event: FredaEvent = { Title: this.eventForm.get('Title').value, Date: this.createDate(), Time: this.eventForm.get('Date').value, Dishes: this.Dishes, userAccess: this.userAccess };
      this.loading = true;
      let ref = this.db.createEventID();
      const id = ref.id
      this.db.newEvent(event, id).then(a => {
        this.loading = false;
        this.router.navigateByUrl('event/' + id);
      }, b => {
        alert('There was a problem while trying to create the event')
        this.loading = false;
      })
    }
    else {
      alert('There was a problem while trying to create the event')
    }
  }
  toggleShow(id: number) {
    this.show[id] = !this.show[id];
  }
  createDate(): Date {
    const dateString = this.eventForm.get('Date').value;
    const dateStrings = dateString.split('/');
    return new Date(Date.UTC(parseInt(dateStrings[2]), parseInt(dateStrings[0]), parseInt(dateStrings[1])));
  }
  checkValue(id: string) {
    const index = this.userAccess.indexOf(id);
    if (index > -1) {
      this.userAccess.splice(index, 1);
    } else {
      this.userAccess.push(id);
    }
  }
}
