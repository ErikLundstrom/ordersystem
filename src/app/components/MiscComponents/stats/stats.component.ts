import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { foodStats, foodCounter, FredaEvent } from 'src/app/interfaces/interfaces';
import bulmaCalendar from "node_modules/bulma-calendar/dist/js/bulma-calendar.min.js";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.sass']
})
export class StatsComponent implements OnInit {

  constructor(private db: DatabaseService, private auth: AuthService) { }
  currentIndex: number;
  allFoodStats: foodStats[];
  allEvents: FredaEvent[];
  foodCounter: foodCounter;
  fromDate: string;
  toDate: string;
  meanTotal: foodCounter;
  meanServed: foodCounter;
  objectKeys = Object.keys;
  icons = { meat: 'drumstick-bite', fish: 'fish', veg: 'carrot', dessert: 'ice-cream' };
  Subscriptions: Subscription[] = [];
  public dateForm = new FormGroup({
    Date: new FormControl(''),
  });

  ngOnInit() {
    this.datePicker()
    this.Subscriptions.push(this.db.ngUnsubscribe$.subscribe(x => this.unSubscribe()))
  }

  ngOnDestroy() {
    this.unSubscribe();
  }
  unSubscribe() {
    this.Subscriptions.map(x => x.unsubscribe());
  }
  datePicker() {
    // Initialize all input of date type.
    const calendars = bulmaCalendar.attach('[type="date"]');
    // Loop on each calendar initialized
    for (var i = 0; i < calendars.length; i++) {
      // Add listener to date:selected event
      calendars[i].on('select', (datepicker => {
        this.dateForm.get('Date').setValue(datepicker.data.value());
      }));
    }
  }
  getStats() {
    this.foodCounter = { meat: 0, fish: 0, veg: 0, dessert: 0 }
    this.currentIndex = 0;
    this.toDate = "";
    this.fromDate = ""
    const input: string = this.dateForm.get('Date').value;
    this.dateForm.get('Date').setValue(null);
    if (input) {
      const inputArray = (input.split(' - '));
      const fromDate = this.toDateFunction(inputArray[0]);
      this.fromDate = inputArray[0];
      let toDate = new Date();
      if (inputArray.length > 1) {
        toDate = this.toDateFunction(inputArray[1]);
        this.toDate = inputArray[1];
      }
      this.Subscriptions.push(this.db.getEventDates(fromDate, toDate).subscribe(events => this.setEvents(events)))
      this.Subscriptions.push(this.db.FoodStats(fromDate, toDate).subscribe(stats => this.setFoodStats(stats)))
    } else {
      this.Subscriptions.push(this.auth.getUid().subscribe(user => {
        this.Subscriptions.push(this.db.getEvents(user.uid).subscribe(events => this.setEvents(events)))
        this.Subscriptions.push(this.db.allFoodStats().subscribe(stats => this.setFoodStats(stats))
        )
      }))
    }

  }
  setFoodStats(stats: foodStats[]) {
    this.allFoodStats = stats;
    this.allFoodStats.reverse();
    this.foodCounter = this.countAll();
    this.meanStats();
  }
  setEvents(events: FredaEvent[]) {
    this.allEvents = events;
    this.allEvents.reverse();
  }

  toDateFunction(dateString: string): Date {
    let dateStrings = dateString.split('/');
    const date = new Date(Date.UTC(parseInt(dateStrings[2]), parseInt(dateStrings[0]), parseInt(dateStrings[1])));
    return date;
  }
  countAll() {
    let counter: foodCounter = { meat: 0, fish: 0, veg: 0, dessert: 0 }
    this.allFoodStats.map(order => {
      counter.meat += order.meat;
      counter.fish += order.fish;
      counter.veg += order.veg;
      counter.dessert += order.dessert;
    })
    return counter;
  }
  nextEvent() {
    if (this.currentIndex < this.allFoodStats.length - 1) {
      this.currentIndex++;
    }
    else {
      alert('There are no more events at the moment.')
    }
  }

  prevEvent() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
    else {
      alert('There are no more events at the moment.')
    }
  }
  meanStats() {
    let meanTotal: foodCounter = { meat: 0, fish: 0, veg: 0, dessert: 0 }
    let meanServed: foodCounter = this.countAll();
    this.allEvents.map(event => {
      event.Dishes.map(dish => meanTotal[dish.Type] += dish.Amount);
    })
    meanTotal.meat /= this.allEvents.length;
    meanTotal.fish /= this.allEvents.length;
    meanTotal.veg /= this.allEvents.length;
    meanTotal.dessert /= this.allEvents.length;

    meanServed.meat /= this.allEvents.length;
    meanServed.fish /= this.allEvents.length;
    meanServed.veg /= this.allEvents.length;
    meanServed.dessert /= this.allEvents.length;
    this.meanServed = meanServed;
    this.meanTotal = meanTotal;
  }
}



