import { Component, OnInit, Input } from '@angular/core';
import { FredaEvent, drinkOrder, drinkCounter } from 'src/app/interfaces/interfaces';
import { DatabaseService } from 'src/app/services/database.service';
import { CountDrinksServices } from 'src/app/services/count-drinks.service';

@Component({
  selector: 'app-barorders',
  templateUrl: './barorders.component.html',
  styleUrls: ['./barorders.component.sass']
})
export class BarordersComponent implements OnInit {
  @Input() fredaEvent: FredaEvent;
  Orders: drinkOrder[];
  counter: drinkCounter;
  counterServed: drinkCounter;
  currentIndex: number = 0;
  icons = { 'beer': 'beer', 'alcFree': 'glass-whiskey', 'wine': 'wine-glass-alt', 'cocktail': 'cocktail' }

  constructor(private db: DatabaseService, private countService: CountDrinksServices) { }

  ngOnInit() {
    let status = 'done';
    while (status == 'done' && this.currentIndex < this.Orders.length - 1) {
      if (this.Orders[this.currentIndex].Status == 'done') {
        status = 'done'
        this.currentIndex++;
      } else {
        status = ''
      }
    }
  }

  @Input()
  set drinkOrders(drinkOrders: drinkOrder[]) {
    this.Orders = drinkOrders;
  }
  markAsDone(idx: number) {
    this.db.updateDrinkOrder({ Status: 'done' }, this.Orders[idx].orderID);
  }
  done(idx: number) {
    return this.Orders[idx].Status == 'done'
  }
  nextOrder() {
    if (this.currentIndex < this.Orders.length - 1) {
      this.currentIndex++;
    }
    else {
      alert('There are no more orders at the moment.')
    }
  }

  prevOrder() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
    else {
      alert('There are no more orders at the moment.')
    }
  }
  count(type: string, order: drinkOrder) {
    return this.countService.count(type, order)
  }
  countAll() {
    this.counter = this.countService.countAll(this.Orders);
  }
  countAllServed() {
    this.counterServed = this.countService.countAllServed(this.Orders);
  }
}
