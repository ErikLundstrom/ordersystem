import { Injectable } from '@angular/core';
import { AngularFirestore, } from '@angular/fire/firestore';
import { FirebaseApp, } from '@angular/fire';
import { map, takeUntil } from 'rxjs/operators';
import { FredaEvent, foodOrder, drinkOrder, foodStats, foodCounter, User } from '../interfaces/interfaces';
import { Observable, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  public ngUnsubscribe$: Subject<void> = new Subject();

  constructor(private afs: AngularFirestore, private auth: AngularFireAuth, private fb: FirebaseApp) { }
  //USERS
  getUser(id: string): Observable<User> {
    return this.afs.collection('/users').doc<User>(id).valueChanges();
  }
  getUsers() {
    return this.afs.collection("/users").snapshotChanges().pipe(
      map(actions => actions.map(a => {
        return a.payload.doc.data() as User;
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }

  setUser(data: User, id: string) {
    return this.afs.collection('/users').doc(id).set(Object.assign({}, data), { merge: true })
  }
  // FOODORDERS
  //-----------------------------------------------------------------------
  //Get all foodOrders for one event
  getFoodOrders(id: string) {
    return this.afs.collection('/foodOrders', ref => ref.orderBy('Time').where('EventID', '==', id)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as foodOrder;
        data.orderID = a.payload.doc.id;
        return { ...data };
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }
  //-----------------------------------------------------------------------
  //Add new order, and add amount of each dish to foodStats
  newFoodOrder(data, foodCounter: foodCounter, foodStats: foodStats, id: string) {
    let stats: foodStats = {
      Time: foodStats.Time, Date: foodStats.Date, eventID: foodStats.eventID, meat: foodStats.meat + foodCounter.meat,
      fish: foodStats.fish + foodCounter.fish, veg: foodStats.veg + foodCounter.veg, dessert: foodStats.dessert + foodCounter.dessert
    }
    let batch = this.afs.firestore.batch();
    let statsRef = this.afs.firestore.collection('/foodStats').doc(id)
    let orderRef = this.afs.firestore.collection('/foodOrders').doc()
    batch.set(orderRef, Object.assign({}, data))
    batch.set(statsRef, Object.assign({}, stats))
    return batch.commit();
  }
  //-----------------------------------------------------------------------
  //Update order
  updateFoodOrder(data, id: string) {
    return this.afs.collection('/foodOrders').doc(id).update(Object.assign({}, data));
  }
  //-----------------------------------------------------------------------
  //Delete order
  deleteFoodOrder(id: string) {
    return this.afs.collection('/foodOrders').doc(id).delete();
  }
  // DRINKORDERS
  //-----------------------------------------------------------------------
  //Get all drinkOrders for one event
  getDrinkOrders(id: string) {
    return this.afs.collection('/drinkOrders', ref => ref.orderBy('Time').where('EventID', '==', id)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as drinkOrder;
        data.orderID = a.payload.doc.id;
        return { ...data };
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }
  //-----------------------------------------------------------------------
  //Add new order
  newDrinkOrder(data) {
    return this.afs.collection('/drinkOrders').add(Object.assign({}, data));
  }
  //-----------------------------------------------------------------------
  //Update order
  updateDrinkOrder(data, id: string) {
    return this.afs.collection('/drinkOrders').doc(id).update(Object.assign({}, data));
  }
  //-----------------------------------------------------------------------
  //Delete order
  deleteDrinkOrder(id: string) {
    return this.afs.collection('/drinkOrders').doc(id).delete();
  }
  //EVENTS
  //-----------------------------------------------------------------------
  //Get all events
  getEvents(id: string) {
    return this.afs.collection('/events', ref => ref.where('userAccess', 'array-contains', id).orderBy('Date', 'desc').limit(10)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as FredaEvent;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }
  //-----------------------------------------------------------------------
  //Get event
  getEvent(id: string): Observable<FredaEvent> {
    return this.afs.collection('/events').doc<FredaEvent>(id).valueChanges().pipe(takeUntil(this.ngUnsubscribe$));
  }
  getEventDates(fromDate: Date, toDate: Date): Observable<FredaEvent[]> {
    return this.afs.collection('/events', ref => ref.where("Date", ">=", fromDate).where("Date", "<=", toDate).orderBy('Date', 'desc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as FredaEvent;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }
  //-----------------------------------------------------------------------
  //Create new event
  createEventID() {
    return this.afs.firestore.collection('/events').doc()
  }

  newEvent(data: FredaEvent, id: string) {
    console.log(data, id);
    let batch = this.afs.firestore.batch();
    let eventRef = this.afs.firestore.collection('/events').doc(id)
    let statsRef = this.afs.firestore.collection('/foodStats').doc()
    let foodStats: foodStats = { Date: data.Date, Time: data.Time, eventID: id, meat: 0, fish: 0, veg: 0, dessert: 0 }
    data.stats = statsRef.id;
    batch.set(eventRef, Object.assign({}, data))
    batch.set(statsRef, Object.assign({}, foodStats))
    return batch.commit();
  }
  //-----------------------------------------------------------------------
  //Delete event
  deleteEvent(id: string) {
    return this.afs.collection('/events').doc(id).delete();
  }
  //FOODSTATS
  //-----------------------------------------------------------------------
  //Get foodstats for event with ID
  getFoodStats(id: string) {
    return this.afs.collection('/foodStats').doc<foodStats>(id).valueChanges().pipe(takeUntil(this.ngUnsubscribe$));
  }
  FoodStats(fromDate: Date, toDate: Date): Observable<foodStats[]> {
    return this.afs.collection('/foodStats', ref => ref.where("Date", ">=", fromDate).where("Date", "<=", toDate).orderBy('Date', 'desc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as foodStats;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }
  allFoodStats(): Observable<foodStats[]> {
    return this.afs.collection('/foodStats', ref => ref.orderBy('Date', 'desc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as foodStats;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    ).pipe(takeUntil(this.ngUnsubscribe$));
  }
}
