import { Injectable } from '@angular/core';
import { foodOrder, foodCounter } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CountDishesService {

  constructor() { }

  count(type: string, order: foodOrder) {
    let counter: number = 0;
    order.Foods.map(x => {
      if (x.Type == type) {
        counter++;
      }
    });
    return counter;
  }
  countAll(Orders:foodOrder[]): foodCounter {
    let counter:foodCounter = { meat: 0, fish: 0, veg: 0, dessert: 0 };
    Orders.map(order => {
      counter.fish += this.count('fish', order);
      counter.meat += this.count('meat', order);
      counter.veg += this.count('veg', order);
      counter.dessert += this.count('dessert', order);
    })
    return counter;
  }
  countAllServed(Orders:foodOrder[]):foodCounter {
    let counterServed:foodCounter = { meat: 0, fish: 0, veg: 0, dessert: 0 };
    Orders.map(order => {
      if (order.Status == 'done') {
        counterServed.fish += this.count('fish', order);
        counterServed.meat += this.count('meat', order);
        counterServed.veg += this.count('veg', order);
        counterServed.dessert += this.count('dessert', order);
      }
    })
    return counterServed;
  }
}

