import { Injectable } from '@angular/core';
import { drinkOrder, drinkCounter } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CountDrinksServices {

  constructor() { }

  count(type: string, order: drinkOrder) {
    let counter: number = 0;
    order.Drinks.map(x => {
      if (x.Type == type) {
        counter++;
      }
    });
    return counter;
  }
  countAll(Orders: drinkOrder[]): drinkCounter {
    let counter: drinkCounter = { 'beer': 0, 'wine': 0, 'cocktail': 0, 'alcFree': 0 };
    Orders.map(order => {
      counter.beer += this.count('beer', order);
      counter.wine += this.count('wine', order);
      counter.cocktail += this.count('cocktail', order);
      counter.alcFree += this.count('alcFree', order);
    })
    return counter;
  }
  countAllServed(Orders: drinkOrder[]): drinkCounter {
    let counterServed: drinkCounter = { 'beer': 0, 'wine': 0, 'cocktail': 0, 'alcFree': 0 };
    Orders.map(order => {
      if (order.Status == 'done') {
        counterServed.beer += this.count('beer', order);
        counterServed.wine += this.count('wine', order);
        counterServed.cocktail += this.count('cocktail', order);
        counterServed[('alcohol-free')] += this.count('alcohol-free', order);
      }
    })
    return counterServed;
  }
}

