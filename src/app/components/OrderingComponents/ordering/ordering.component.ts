import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FredaEvent, foodStats } from 'src/app/interfaces/interfaces';
import { Subscription } from 'rxjs';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-ordering',
  templateUrl: './ordering.component.html',
  styleUrls: ['./ordering.component.sass']
})
export class orderingComponent implements OnInit, OnDestroy {
  @Input() fredaEvent: FredaEvent;
  tab: boolean = true;
  Subscriptions: Subscription[] = [];
  foodStats: foodStats;
  constructor(public db: DatabaseService) { }

  ngOnInit() {
    this.Subscriptions.push(this.db.ngUnsubscribe$.subscribe(x => this.unSubscribe()))
    this.Subscriptions.push(this.db.getFoodStats(this.fredaEvent.stats).subscribe(stats => {
      this.foodStats = stats;
    }));
  }
  ngOnDestroy() {
    this.unSubscribe();
  }
  unSubscribe() {
    this.Subscriptions.map(x => x.unsubscribe());
  }
  toggleTab(bool: boolean) {
    this.tab = bool;
  }

}