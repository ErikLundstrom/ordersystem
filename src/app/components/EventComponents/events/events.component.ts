import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router'
import { DatabaseService } from 'src/app/services/database.service';
import { FredaEvent } from 'src/app/interfaces/interfaces';
import { AuthService } from 'src/app/services/auth.service';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.sass']
})
export class EventsComponent implements OnInit, OnDestroy {
  events: FredaEvent[];
  myFilter: string;
  Subscriptions: Subscription[] = [];

  constructor(public router: Router, public db: DatabaseService, public auth: AuthService) { }

  ngOnInit() {
    this.Subscriptions.push(this.db.ngUnsubscribe$.subscribe(x => this.unSubscribe()))
    this.Subscriptions.push(this.auth.getUid().subscribe(user =>
      this.Subscriptions.push(this.db.getEvents(user.uid).subscribe(e => this.events = e))))
  }
  ngOnDestroy() {
    this.unSubscribe();
  }
  public unSubscribe() {
    this.Subscriptions.map(x => x.unsubscribe());
  }
  filter(event: FredaEvent) {
    return (event.Title.toLowerCase().includes(this.myFilter.toLowerCase()) || event.Time.toLowerCase().includes(this.myFilter.toLowerCase()))
  }
}

