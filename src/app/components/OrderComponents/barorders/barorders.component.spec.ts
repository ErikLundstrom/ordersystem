import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarordersComponent } from './barorders.component';

describe('BarordersComponent', () => {
  let component: BarordersComponent;
  let fixture: ComponentFixture<BarordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
