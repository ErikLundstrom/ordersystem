import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  public show: boolean = false;
  constructor(public authService: AuthService) { }

  ngOnInit() {
  }
  toggle() {
    this.show = !this.show
  }
  logOut() {
    this.toggle();
    this.authService.logout();
  }


}
