import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { DatabaseService } from './database.service';
import { Subscription } from 'rxjs';
import { LoaderService } from './loader.service';
import { User } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: User;
  public uid: string;
  private admin: boolean;
  private userCheck: Subscription;

  constructor(public afAuth: AngularFireAuth, public router: Router, private db: DatabaseService, private loaderService: LoaderService) {
    this.loggedin();
  }
  loggedin() {
    this.loaderService.load()
    if (this.userCheck) {
      this.userCheck.unsubscribe();
    }
    this.userCheck = this.afAuth.authState.subscribe(user => {
      if (user) {
        localStorage.setItem('user', JSON.stringify(user));
        const dbUser: User = { 'email': user.email, 'lastLogin': new Date(), 'uid': user.uid }
        this.db.setUser(dbUser, user.uid);
        this.userCheck.add(this.userCheck.add(this.db.getUser(user.uid).subscribe(user => {
          this.user = user;
        })));
      } else {
        localStorage.setItem('user', null);
        this.user = null;
      }
      this.loaderService.done();
    })
  }
  async  login(email: string, password: string) {
    this.loaderService.load();
    try {
      await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      await this.loggedin();

      this.router.navigate(['events']);
    } catch (e) {
      alert("Error!" + e.message);
    }
    this.loaderService.done();
  }
  logout() {
    this.db.ngUnsubscribe$.next();
    this.db.ngUnsubscribe$.complete();
    this.userCheck.unsubscribe();
    this.logOut();
  }
  async logOut() {
    await this.afAuth.auth.signOut();
    this.admin = false;
    localStorage.removeItem('user');
    this.router.navigate(['']);

  }
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
  Admin() {
    if (this.user){
      return this.user.role !== null;
    } else return false;
  }
  async  resetPassword(email: string) {
    this.afAuth.auth.sendPasswordResetEmail(email).then(function () {
      alert('An email with instructions has been sent to: ' + email + '.');
    }).catch(function (error) {
      alert("Error!" + error.message);

    });
  }
  getUid(){
    return this.afAuth.user;
  }
}
