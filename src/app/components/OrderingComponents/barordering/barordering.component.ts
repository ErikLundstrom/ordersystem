import { Component, OnInit, Input } from '@angular/core';
import { FredaEvent, drinkOrder, Drink } from 'src/app/interfaces/interfaces';
import { DatabaseService } from 'src/app/services/database.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-barordering',
  templateUrl: './barordering.component.html',
  styleUrls: ['./barordering.component.sass']
})
export class BarorderingComponent implements OnInit {
  @Input() fredaEvent: FredaEvent;
  Table: string;
  drinks: Drink[] = [];
  drink: string[] = ['', '', '', ''];
  types: string[] = ['beer', 'wine', 'cocktail', 'alcFree']
  beer = {};
  wine = {};
  cocktail = {};
  alcFree = {};
  fluids = [this.beer, this.wine, this.cocktail, this.alcFree]
  constructor(public db: DatabaseService, private loaderService:LoaderService) { }

  ngOnInit() {
  }

  addDrink(id: number) {
    if (this.drink[id]) {
      const drink = { Title: this.drink[id], Type: this.types[id] }
      this.drinks.unshift(drink);
      this.drink[id] = "";
    } else {
      alert('Pick a drink before you try to add it.')
    }
  }
  deleteDrink(index:number){
    this.drinks.splice(index, 1);
  }
  saveOrder() {
    if (this.drinks.length > 0 && this.Table) {
      this.loaderService.load();
      const order: drinkOrder = { Time: new Date(), EventID: this.fredaEvent.id, Drinks: this.drinks, Table: this.Table }
      this.db.newDrinkOrder(order).then(() => {
        this.drinks = [];
        this.Table = '';
        this.loaderService.done();
      }).catch(function (error) {
        alert("Error with ordering: " + error);
        this.loading = false;
      })
    }
  }

}