import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { orderingComponent } from './components/OrderingComponents/ordering/ordering.component';
import { EventComponent } from './components/EventComponents/event/event.component';
import { StatsComponent } from './components/MiscComponents/stats/stats.component';
import { BarordersComponent } from './components/OrderComponents/barorders/barorders.component';
import { KitchenordersComponent } from './components/OrderComponents/kitchenorders/kitchenorders.component';
import { BarorderingComponent } from './components/OrderingComponents/barordering/barordering.component';
import { KitchenorderingComponent } from './components/OrderingComponents/kitchenordering/kitchenordering.component';
import { CreateEventComponent } from './components/EventComponents/create-event/create-event.component';
import { LoginComponent } from './components/MiscComponents/login/login.component';
import { HeaderComponent } from './components/MiscComponents/header/header.component';
import { EventsComponent } from './components/EventComponents/events/events.component';
import { StartComponent } from './components/MiscComponents/start/start.component';
import { ordersComponent } from './components/OrderComponents/orders/orders.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import {
  faUtensils, faKey, faGlassMartini, faReceipt, faCalendar,
  faQuestion, faChartBar, faHashtag, faListOl, faFish, faDrumstickBite,
  faCarrot, faLock, faUser, faSignOutAlt, faSignInAlt, faPlus, faMinus, faFilter,
  faIceCream, faCheck, faBeer, faCocktail, faGlassWhiskey, faWineGlassAlt,faImage, fas
} from '@fortawesome/free-solid-svg-icons';
import { LoaderComponent } from './components/MiscComponents/loader/loader.component';
import { ServiceWorkerModule } from '@angular/service-worker';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EventsComponent,
    StartComponent,
    ordersComponent,
    orderingComponent,
    EventComponent,
    StatsComponent,
    BarordersComponent,
    KitchenordersComponent,
    BarorderingComponent,
    KitchenorderingComponent,
    CreateEventComponent,
    LoginComponent,
    LoaderComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
constructor(library: FaIconLibrary) {
      +   library.addIconPacks(fas);
    library.addIcons(faUtensils, faKey, faGlassMartini, faReceipt,
      faCalendar, faQuestion, faChartBar, faHashtag, faListOl,
      faFish, faDrumstickBite, faCarrot, faLock, faUser, faSignOutAlt,
      faSignInAlt, faPlus, faMinus, faFilter, faIceCream, faCheck,
      faBeer, faCocktail, faGlassWhiskey, faWineGlassAlt, faImage);
  }
}
