import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { FredaEvent } from 'src/app/interfaces/interfaces';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.sass']
})
export class EventComponent implements OnInit, OnDestroy {
  tab: boolean = true;
  id: string;
  fredaEvent: FredaEvent;
  Subscriptions: Subscription[] = [];
  FREDA: Observable<FredaEvent>;


  constructor(private route: ActivatedRoute, private router: Router, private db: DatabaseService) { }

  ngOnInit() {
    this.Subscriptions.push(this.db.ngUnsubscribe$.subscribe(x => this.unSubscribe()))
    this.Subscriptions.push(this.route.params.subscribe(
      params => {
        this.id = params['id'];
        this.Subscriptions.push(this.db.getEvent(this.id).subscribe((event) => {
          this.fredaEvent = event;
          this.fredaEvent.id = this.id;
        }));
      }));
  }
  ngOnDestroy() {
    this.unSubscribe();
  }
  public unSubscribe() {
    this.Subscriptions.map(x => {
      x.unsubscribe();
    });
  }

  toggleTab(bool:boolean) {
    this.tab = bool;
  }
}
