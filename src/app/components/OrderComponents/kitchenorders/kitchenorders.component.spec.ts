import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenordersComponent } from './kitchenorders.component';

describe('KitchenordersComponent', () => {
  let component: KitchenordersComponent;
  let fixture: ComponentFixture<KitchenordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KitchenordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
