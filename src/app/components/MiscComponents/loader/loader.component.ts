import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor(private loaderService:LoaderService) { }
  loading: boolean = false;

  ngOnInit() {
    this.loaderService.loader$.subscribe(x => this.loading = x)
  }

}
