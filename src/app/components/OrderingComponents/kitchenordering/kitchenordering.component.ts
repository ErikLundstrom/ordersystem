import { Component, OnInit, Input } from '@angular/core';
import { Food, Dish, FredaEvent, foodOrder, foodCounter, foodStats } from 'src/app/interfaces/interfaces';
import { DatabaseService } from 'src/app/services/database.service';
import { CountDishesService } from 'src/app/services/count-dishes.service';
import { LoaderService } from 'src/app/services/loader.service';


@Component({
  selector: 'app-kitchenordering',
  templateUrl: './kitchenordering.component.html',
  styleUrls: ['./kitchenordering.component.sass']
})
export class KitchenorderingComponent implements OnInit {
  @Input() fredaEvent: FredaEvent;
  @Input() foodStats: foodStats;
  Table: string;
  dishes: Dish[];
  foods: Food[] = [];
  currentDish: string = "";
  show: boolean[] = [];
  icons = { meat: 'drumstick-bite', fish: 'fish', veg: 'carrot', dessert: 'ice-cream' };
  limitCounter: foodCounter = { meat: 0, fish: 0, veg: 0, dessert: 0 }
  objectKeys = Object.keys;

  constructor(private db: DatabaseService, private countService: CountDishesService, private loaderService: LoaderService) { }

  ngOnInit() {
    this.dishes = this.fredaEvent.Dishes;
    this.dishes.map(x => this.limitCounter[x.Type] = x.Amount)
  }

  toggleShow(id: string) {
    this.show[id] = !this.show[id];
  }
  addDish() {
    let dish: Dish;
    this.dishes.forEach(x => {
      if (x.Type == this.currentDish) {
        dish = x;
      }
    })
    if (dish) {
      if (this.compare(dish.Type)) {
        if (dish) {
          let Allergies = {};
          dish.Allergies.forEach(x => { Allergies[x] = false })
          const food: Food = { Type: dish.Type, Allergies: Allergies }
          this.foods.unshift(food);
          this.show.unshift(false);
        } else {
          alert('Add a dish before you try to add it.')
        }
      } else {
        alert('There are no more of "' + dish.Type + '". You need to choose something else')
      }
    }
  }
  saveOrder() {
    let valid = true;
    if (this.foods.length > 0 && this.Table) {
      const order: foodOrder = { Time: new Date(), EventID: this.fredaEvent.id, Foods: this.foods, Table: this.Table }
      let count = this.countService.countAll([order]);
      Object.keys(count).map((x) => {
        if (this.limitCounter[x] >= count[x] + this.foodStats[x]) {
        } else {
          valid = false
          alert('You have tried to order more dishes of type "' + x + '" than there are available today.(' + (count[x] + this.foodStats[x]) + ' of ' + this.limitCounter[x] + ' available.)')
        }
      })
      if (valid) {
        let counter = this.countService.countAll([order]);
        this.loaderService.load()
        this.db.newFoodOrder(order, counter, this.foodStats, this.fredaEvent.stats).then(() => {
          this.foods = [];
          this.Table = '';
          this.loaderService.done()
        })
          .catch(function (error) {
            alert("Error with ordering: " + error);
            this.loading = false;
          });
      }
    }
  }

  deleteDish(index: number) {
    this.foods.splice(index, 1);
    this.show.splice(index, 1);
  }

  compare(type: string): boolean {
    return this.limitCounter[type] > this.foodStats[type] + this.countService.count(type, { Time: new Date(), EventID: this.fredaEvent.id, Foods: this.foods, Table: this.Table });
  }
  count(type: string): number {
    return this.foodStats[type] + this.countService.count(type, { Time: new Date(), EventID: this.fredaEvent.id, Foods: this.foods, Table: this.Table });
  }
  textWarnings(dish) {
    if (dish.Amount - this.count(dish.Type) <= 3) {
      return 0;
    }
    else if (dish.Amount - this.count(dish.Type) > 0 && dish.Amount - this.count(dish.Type) <= 10) {
      return 1;
    }
    else {
      return 2;
    }
  }
}