import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public loader$: Subject<boolean> = new Subject();

  constructor() { }

  load(){
    this.loader$.next(true);
  }
  done(){
    this.loader$.next(false);
  }
}
