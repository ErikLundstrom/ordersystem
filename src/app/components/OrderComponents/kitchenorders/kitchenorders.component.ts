import { Component, OnInit, Input } from '@angular/core';
import { foodOrder, FredaEvent, foodCounter } from 'src/app/interfaces/interfaces';
import { DatabaseService } from 'src/app/services/database.service';
import { CountDishesService } from 'src/app/services/count-dishes.service';


@Component({
  selector: 'app-kitchenorders',
  templateUrl: './kitchenorders.component.html',
  styleUrls: ['./kitchenorders.component.sass']
})
export class KitchenordersComponent implements OnInit {
  @Input() fredaEvent: FredaEvent;
  currentIndex: number = 0;
  public Orders: foodOrder[];
  public icons = { meat: 'drumstick-bite', fish: 'fish', veg: 'carrot', dessert: 'ice-cream' };
  public counter: foodCounter;
  public counterServed: foodCounter;
  public objectKeys = Object.keys;
  constructor(private db: DatabaseService, private countService: CountDishesService) { }

  @Input()
  set foodOrders(foodOrders: foodOrder[]) {
    this.Orders = foodOrders;
    this.countAll();
    this.countAllServed();
  }

  ngOnInit() {
    let status = 'done';
    while (status == 'done' && this.currentIndex < this.Orders.length - 1) {
      if (this.Orders[this.currentIndex].Status == 'done') {
        status = 'done'
        this.currentIndex++;
      } else {
        status = ''
      }
    }
  }

  markAsDone(idx: number) {
    this.db.updateFoodOrder({ Status: 'done' }, this.Orders[idx].orderID).then(() => this.countAllServed());
  }
  done(idx: number) {
    return this.Orders[idx].Status == 'done'
  }
  nextOrder() {
    if (this.currentIndex < this.Orders.length - 1) {
      this.currentIndex++;
    }
    else {
      alert('There are no more orders at the moment.')
    }
  }
  prevOrder() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
    else {
      alert('There are no more orders at the moment.')
    }
  }
  count(type: string, order: foodOrder) {
    return this.countService.count(type, order)
  }
  countAll() {
    this.counter = this.countService.countAll(this.Orders);
  }
  countAllServed() {
    this.counterServed = this.countService.countAllServed(this.Orders);
  }
}
