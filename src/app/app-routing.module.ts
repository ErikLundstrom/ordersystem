import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './components/MiscComponents/start/start.component'
import { EventsComponent } from './components/EventComponents/events/events.component';
import { EventComponent } from './components/EventComponents/event/event.component';
import { CreateEventComponent } from './components/EventComponents/create-event/create-event.component';
import { Guard } from './guards/guard.guard';
import { StatsComponent } from './components/MiscComponents/stats/stats.component';

const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'events', component: EventsComponent, canActivate: [Guard] },
  { path: 'event/:id', component: EventComponent, canActivate: [Guard] },
  { path: 'newevent', component: CreateEventComponent, canActivate: [Guard] },
  { path: 'stats', component: StatsComponent, canActivate: [Guard] }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
